import tensorflow as tf
import PIL
import numpy as np
import cv2
import os
from PIL import Image
import eel
import base64
import time
start = time.time()

global flag
flag = 0


@eel.expose
def edited():
    if flag == 1:
        return "Вычисления выполнены"
    else:
        return "Не прошло"


@eel.expose
def addfile(file):
    print(file[0:10])
    file = file[22:]
    byte = base64.b64decode(file)
    path = "base64.jpg"  # сюда делать путь для картинки
    with open(path, 'wb') as b64:
        b64.write(byte)  # картинка идет на диск

    def crop(infile, height=300, width=300):
        a = []
        im = Image.open(infile)
        imgwidth, imgheight = im.size
        for i in range(imgheight // height):
            for j in range(imgwidth // width):
                box = (j * width, i * height, (j + 1)
                       * width, (i + 1) * height)
                a.append(im.crop(box))
        return a
    img = cv2.imread(path)  # считывание картинки
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    labelnum, labelimg, contours, GoCs = cv2.connectedComponentsWithStats(gray)
    for label in range(1, labelnum):
        x, y, w, h, size = contours[label]
        if size <= 50:
            img[y:y+h, x:x+w] = 0
    cv2.imwrite("img.JPG", img)
    interpreter = tf.lite.Interpreter(model_path="./model.tflite")
    interpreter.allocate_tensors()
    a = crop(os.path.join("./img.JPG"))
    os.mkdir("./new")
    os.chdir("./new")
    n = 0
    for i in a:
        i.save(str(n), 'jpeg')
        n += 1
    s = os.listdir("./")

    def mus(s, intr=interpreter):
        input_index = interpreter.get_input_details()[0]["index"]
        output_index = interpreter.get_output_details()[0]["index"]
        cnt = 0
        ans = []
        for i in s:
            test_image = np.expand_dims(cv2.imread(i), axis=0).astype(np.uint8)
            interpreter.set_tensor(input_index, test_image)
            interpreter.invoke()
            output = interpreter.tensor(output_index)
            cnt += np.argmax(output()[0])
            if (np.argmax(output()[0])):
                ans.append(i)
        return bool(cnt), ans
    res = mus(s)
    if res[0]:
        print("Здесь есть медведь")
        print(*res[1])
        end = time.time()
        print("\n"+str(round(end - start, 3)) + " seconds need to find bear")
        flag += 1
    else:
        #print("Здесь нет медведей")
        end = time.time()
        print("\n"+str(round(end - start, 3)) + " seconds need to find bear")
        eel.edited_png()


eel.init('front')
eel.start('index.html', port=8000, size=(1000, 800))
