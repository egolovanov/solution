Добро пожаловать на репозиторий команды Летней школы от Цифрового Прорыва "О_о". 
Здесь вы найдете исходный код приложения и Jupyter ноутбуки создания модели и тестирования на работоспособность.
При создании модели учитывались потребности и возмрожности кейсодержателя, поэтому предоставлено две модели: без квантования https://drive.google.com/file/d/1JGd3JIGHfwgpRhzTL9B-bNqFk43UU8Oi/view?usp=sharing
и с квантованием float16 https://drive.google.com/file/d/1DyUQycaQqXcR79QwDQ1djdSHxsUax0t9/view?usp=sharing .
Узнать подробнее о квантовании моделей в TensorFlow можно здесь https://www.tensorflow.org/lite/performance/post_training_quantization
Почему модель надо скачивать отдельно от репозитория? Это сделано для того чтобы вы могли выбрать между двумя приведенными моделями выше и поняли в чем их отличия и, соответственно, выбрали в соответствии со своими потребностям
Обязательно наличие Chrome! или другого браузера на базе Chronium
+ для за пуска приложений необходим установленный Python 3.x и библиотеки:
pip install opencv-python
pip install Eel
pip install numpy
pip install tensorflow
pip install pillow